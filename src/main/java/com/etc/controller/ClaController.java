package com.etc.controller;

import com.etc.entity.Cla;
import com.etc.service.ClaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class ClaController {
    @Autowired
    private ClaService claService;

    @RequestMapping("/claquery")
    public ModelAndView query(@RequestParam(defaultValue = "1") Integer pageNum,
                              @RequestParam(defaultValue = "5") Integer pageSize){
        ModelAndView mv = new ModelAndView("clashow");
        mv.addObject("p",claService.queryPage(pageNum,pageSize));
        return mv;
    }

    @RequestMapping("/claadd")
    public ModelAndView add(Cla c){
        ModelAndView mv = new ModelAndView(new RedirectView("claquery"));
        int res = claService.add(c);
        mv.addObject("msg",res>0?"新加成功":"新加失败");
        return mv;
    }

    @RequestMapping("/clamod")
    public ModelAndView mod(Cla c){
        ModelAndView mv = new ModelAndView(new RedirectView("claquery"));
        int res = claService.mod(c);
        mv.addObject("msg",res>0?"修改成功":"修改失败");
        return mv;
    }

    @RequestMapping("/cladel")
    public ModelAndView del(Integer cid){
        ModelAndView mv = new ModelAndView(new RedirectView("claquery"));
        int res = claService.del(cid);
        mv.addObject("msg",res>0?"删除成功":"删除失败");
        return mv;
    }
}
