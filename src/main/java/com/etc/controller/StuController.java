package com.etc.controller;

import com.etc.entity.Stu;
import com.etc.service.ClaService;
import com.etc.service.StuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.InternalResourceView;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class StuController {
    @Autowired
    private StuService stuService;
    @Autowired
    private ClaService claService;

    @RequestMapping("/stuquery")
    public ModelAndView query(Stu stu, @RequestParam(defaultValue = "1") Integer pageNum,
                              @RequestParam(defaultValue = "5") Integer pageSize){
        ModelAndView mv = new ModelAndView("stushow");
        mv.addObject("p",stuService.queryPage(stu,pageNum,pageSize));
        mv.addObject("clist",claService.query());
        return mv;
    }

    @RequestMapping("/stuadd")
    public ModelAndView add(Stu s){
        ModelAndView mv = new ModelAndView(new RedirectView("stuquery"));
        int res = stuService.add(s);
        mv.addObject("msg",res>0?"新加成功":"新加失败");
        return mv;
    }

    @RequestMapping("/stumod")
    public ModelAndView mod(Stu s){
        ModelAndView mv = new ModelAndView(new RedirectView("stuquery"));
        int res = stuService.mod(s);
        mv.addObject("msg",res>0?"修改成功":"修改失败");
        return mv;
    }

    @RequestMapping("/studel")
    public ModelAndView del(Integer sid){
        ModelAndView mv = new ModelAndView(new RedirectView("stuquery"));
        int res = stuService.del(sid);
        mv.addObject("msg",res>0?"删除成功":"删除失败");
        return mv;
    }
}
