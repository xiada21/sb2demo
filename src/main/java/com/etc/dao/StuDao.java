package com.etc.dao;

import java.sql.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.etc.entity.Stu;

public interface StuDao {
	public List<Stu> queryAll();
	public Stu get(Integer sid);
	public int add(Stu s);
	public int mod(Stu s);
	public int del(Integer sid);
	public List<Stu> query(String sname);
	public List<Stu> queryByBirth(@Param("start") Date start, @Param("end") Date end);
	public List<Stu> queryByCondition(@Param("sid") Integer sid, @Param("sname") String sname, @Param("sex") String sex);
	public List<Stu> queryByStu(Stu s);
	public List<Stu> queryByIds(@Param("ids") List<Integer> ids);
}
