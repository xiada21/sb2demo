package com.etc.dao;

import org.apache.ibatis.annotations.Param;

import com.etc.entity.User;

public interface UserDao {

	User query(@Param("uname") String uname, @Param("password") String password);
}
