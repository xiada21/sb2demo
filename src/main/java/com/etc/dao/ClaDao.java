package com.etc.dao;

import java.util.List;

import com.etc.entity.Cla;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
public interface ClaDao {

	public List<Cla> query();
	public Cla get(Integer cid);
	int add(Cla c);
	int mod(Cla c);
	int del(Integer cid);
}
