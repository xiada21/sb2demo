package com.etc;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@MapperScan("com.etc.dao")
@EnableCaching
//这是启动类
public class Sb2demoApplication {

	public static void main(String[] args) {
		SpringApplication.run(Sb2demoApplication.class, args);
	}

}
