package com.etc.service;

import java.util.List;

import com.etc.entity.Stu;
import com.github.pagehelper.PageInfo;

public interface StuService {

	int add(Stu s);
	int mod(Stu s);
	int del(Integer sid);
	Stu get(Integer sid);
	List<Stu> query();
	List<Stu> query(Stu s);//s表示封装的查询条件
	//pageNum:当前页 pageSize:表示每页显示多少条记录
	PageInfo<Stu> queryPage(Stu s, Integer pageNum, Integer pageSize);
	
}
