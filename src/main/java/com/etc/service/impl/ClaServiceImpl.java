package com.etc.service.impl;

import com.etc.dao.ClaDao;
import com.etc.entity.Cla;
import com.etc.service.ClaService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClaServiceImpl implements ClaService {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private ClaDao claDao;
	
	@Override
	@Cacheable(value = "getAllClas",key = "1")
	public List<Cla> query() {
		logger.info("这是第一次查询所有的班级............");
		return claDao.query();
	}

	@Override
	@CacheEvict(value = "getAllClas",key = "1")
	public int add(Cla c) {
		return claDao.add(c);
	}

	@Override
	@CacheEvict(value = "getAllClas",key = "1")
	public int mod(Cla c) {
		return claDao.mod(c);
	}

	@Override
	@CacheEvict(value = "getAllClas",key = "1")
	public int del(Integer cid) {
		return claDao.del(cid);
	}

	@Override
	public PageInfo<Cla> queryPage(Integer pageNum, Integer pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		List<Cla> list = claDao.query();
		PageInfo<Cla> page = new PageInfo<Cla>(list);
		return page;
	}

}
