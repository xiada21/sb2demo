package com.etc.service.impl;

import com.etc.dao.StuDao;
import com.etc.entity.Stu;
import com.etc.service.StuService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StuServiceImpl implements StuService {

	@Autowired
	private StuDao stuDao;

	@Override
	public int add(Stu s) {
		int res = stuDao.add(s);
		return res;
	}

	@Override
	public int mod(Stu s) {
		return stuDao.mod(s);
	}

	@Override
	public int del(Integer sid) {
		return stuDao.del(sid);
	}

	@Override
	public Stu get(Integer sid) {
		return stuDao.get(sid);
	}

	@Override
	public List<Stu> query() {
		return stuDao.queryAll();
	}

	@Override
	public List<Stu> query(Stu s) {
		return stuDao.queryByStu(s);
	}

	@Override
	public PageInfo<Stu> queryPage(Stu s, Integer pageNum, Integer pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		List<Stu> list = stuDao.queryByStu(s);
		PageInfo<Stu> page = new PageInfo<Stu>(list);
		return page;
	}
	
}
