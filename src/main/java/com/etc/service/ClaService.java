package com.etc.service;

import java.util.List;

import com.etc.entity.Cla;
import com.github.pagehelper.PageInfo;

public interface ClaService {

	List<Cla> query();
	int add(Cla c);
	int mod(Cla c);
	int del(Integer cid);
	public PageInfo<Cla> queryPage(Integer pageNum, Integer pageSize);
}
