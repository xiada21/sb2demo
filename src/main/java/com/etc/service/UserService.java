package com.etc.service;

import com.etc.entity.User;

public interface UserService {

	User query(String uname, String password);
}
