package com.etc.entity;

import lombok.Data;

@Data
public class User {

	private Integer uid;
	private String uname;
	private String password;
}
