package com.etc.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class Cla implements Serializable {

	private Integer cid;
	private String cname;
	private List<Stu> list;
}
