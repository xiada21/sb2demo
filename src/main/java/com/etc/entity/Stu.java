package com.etc.entity;

import lombok.Data;

import java.sql.Date;

@Data
public class Stu {

	private Integer sid;
	private String sname;
	private String sex;
	private Date birth;
	private Cla cla;//学生关联的班级，用班级实体类的类型 多对一
	
}
