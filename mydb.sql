/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50514
Source Host           : localhost:3306
Source Database       : mydb

Target Server Type    : MYSQL
Target Server Version : 50514
File Encoding         : 65001

Date: 2024-07-12 11:07:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `cla`
-- ----------------------------
DROP TABLE IF EXISTS `cla`;
CREATE TABLE `cla` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `cname` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cla
-- ----------------------------
INSERT INTO `cla` VALUES ('1', 'SJ806');
INSERT INTO `cla` VALUES ('2', 'SJ902');
INSERT INTO `cla` VALUES ('3', 'Java01');
INSERT INTO `cla` VALUES ('4', 'C001');
INSERT INTO `cla` VALUES ('5', '人工智能2班');

-- ----------------------------
-- Table structure for `stu`
-- ----------------------------
DROP TABLE IF EXISTS `stu`;
CREATE TABLE `stu` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `sname` varchar(50) DEFAULT NULL,
  `sex` char(1) DEFAULT NULL,
  `birth` date DEFAULT NULL,
  `cid` int(11) DEFAULT NULL,
  PRIMARY KEY (`sid`),
  KEY `stu_fk_cid` (`cid`),
  CONSTRAINT `stu_fk_cid` FOREIGN KEY (`cid`) REFERENCES `cla` (`cid`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of stu
-- ----------------------------
INSERT INTO `stu` VALUES ('1', '张三', '男', '2000-12-10', '1');
INSERT INTO `stu` VALUES ('2', '张四', '男', '2000-12-10', '1');
INSERT INTO `stu` VALUES ('3', '张三丰', '男', '2004-04-04', '2');
INSERT INTO `stu` VALUES ('5', '王五', '女', '2002-02-02', '1');
INSERT INTO `stu` VALUES ('6', '李嘉欣', '女', '2003-03-03', '2');
INSERT INTO `stu` VALUES ('8', '王五', '女', '2002-02-02', '3');
INSERT INTO `stu` VALUES ('9', '李嘉欣', '女', '2003-03-03', '1');
INSERT INTO `stu` VALUES ('10', '张三', '男', '2000-12-10', '1');
INSERT INTO `stu` VALUES ('11', '李嘉欣', '女', '2000-11-11', '1');
INSERT INTO `stu` VALUES ('12', '张柏芝', '女', '1982-02-05', '1');
INSERT INTO `stu` VALUES ('13', '雷锋', '男', '1999-09-09', '1');
INSERT INTO `stu` VALUES ('16', '李天一', '男', '1999-05-08', '2');
INSERT INTO `stu` VALUES ('18', '李天二', '女', '2000-03-07', '2');
INSERT INTO `stu` VALUES ('19', '李天三', '男', '2000-07-03', '2');
INSERT INTO `stu` VALUES ('20', '陈慧琳', '女', '2007-08-08', '2');
INSERT INTO `stu` VALUES ('21', '白露', '女', '1998-07-05', '3');
INSERT INTO `stu` VALUES ('22', '海灯', '男', '1984-09-30', '4');
INSERT INTO `stu` VALUES ('23', '陈慧琳2', '女', '2024-05-26', '4');
INSERT INTO `stu` VALUES ('27', '李思敏', '女', '1997-07-08', '4');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `uname` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'admin', '123456');
